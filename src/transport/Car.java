package transport;

import media.Radio;

import java.util.Objects;

public  class Car extends Vehicle{
	final int horsePower;
	private Radio player = new Radio();
	public  void  pushGas(){
		speed+=10;
	}

	public void  brake(){
		setSpeed(speed-10);
	}

	public void play(){
		player.play();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Car)) return false;
		Car car = (Car) o;
		return horsePower == car.horsePower;
	}

	@Override
	public int hashCode() {
		return Objects.hash(horsePower);
	}

	public Car(int horsePower) {
		this.horsePower = horsePower;
	}

	public Car() {
		this(50);
		System.out.println("Making a car");
	}

	public int getHorsePower() {
		return horsePower;
	}

	@Override
	public String toString() {
		return "Car{" +
			"horsePower=" + horsePower +
			", speed=" + speed +
			'}';
	}
}
