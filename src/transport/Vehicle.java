package transport;

public abstract class Vehicle {
	protected int speed=0;



	public void setSpeed(int speed) {
		this.speed = speed>=0?speed:0;
	}

	public abstract void  brake();

	public int getSpeed() {
		return speed;
	}


}
