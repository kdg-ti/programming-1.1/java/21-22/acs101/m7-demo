import transport.Car;
import transport.army.AmphibianTank;
import transport.army.Tank;

public class Runner {

	public static void main(String[] args) {
		Car vw = new Car(144);
		vw.pushGas();
		System.out.println("My car is " + vw);
		Tank leopard = new Tank();
		leopard.pushGas();
		leopard.pushGas();
		leopard.pushGas();
		System.out.println("My tank is " + leopard) ;
		leopard.pushGas();
		leopard.pushGas();
		leopard.pushGas();
		System.out.println("Driving at top speed " + leopard) ;
		leopard.pushGas();
		System.out.println("Driving at top speed " + leopard) ;
		AmphibianTank sailor = new AmphibianTank();
		System.out.println("subsubclass" + sailor);
		vw=new Tank();
		System.out.println("Look at my car: "+vw);
		Object o=new Tank();
		System.out.println("Look at my object: "+o);
		Tank realTank = (Tank)vw;
		realTank.fire();
		Object fakeTank = new Object();
		// runtime error!!
	//	realTank = (Tank)fakeTank;
		if (vw instanceof Car){
			System.out.println("vw is a car");
		}
		// classical instanceof
		if (vw instanceof Tank){
			System.out.println("vw is a tank");
			Tank t = (Tank) vw;
			t.fire();
		} else {
			System.out.println("vw is not a Tank");
		}
		// instanceof Java 16
		if (vw instanceof Tank t){
			System.out.println("vw is a tank");
			t.fire();
		} else {
			System.out.println("vw is not a Tank");
		}
		System.out.println("Object o REFERS TO " + o.getClass());
	}
}
