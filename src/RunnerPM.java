import transport.*;
import transport.army.AmphibianTank;
import transport.army.Tank;

public class RunnerPM {
	public static void main(String[] args) {
		Tank t = new Tank();
		// you cannot make objects of an ab stract class
		//Vehicle v = new Vehicle() ;
		Vehicle v = t;
		t.setSpeed(50);
		t.brake();
		System.out.println("My Vehicle: "+ t);
		Vehicle[] vehicles = {new Car(10),new Tank(),new AmphibianTank(),new Bicycle()};
		printall(vehicles);
	}

	private static void printall(Vehicle[] vehicles) {
		for (int i = 0; i < vehicles.length; i++) {
			System.out.println(vehicles[i].toString());
		}
	}
}
